// Joseph John
// C0741494
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.validator.PublicClassValidator;

public class SnakeTest {
	
	//Create object
	Snake snakeOne,snakeTwo;

	@Before
	public void setUp() throws Exception {
		//Make a new snake object
		
		// snakeOne first object
		snakeOne = new Snake("Peter", 10, "coffee");
		
		// snakeTwo second obkect
		snakeTwo = new Snake("Takis", 80, "vegetebles");
		
		
	}

	@After
	public void tearDown() throws Exception {
	}

	//TC1 Testing the isHealthy() function
	@Test
	public void testIsHealthy() {
		
		String result1 = String.valueOf(snakeOne.isHealthy());
		assertEquals(result1,"false");
			
		}
	
	//TC2 Testing the fitsInCage() function
	@Test
	public void testfitsInCage() {
		String result2 = String.valueOf(snakeTwo.fitsInCage(100));
		assertEquals(result2,"true");
			
				
		}
	
		
	}

