
// Joseph John
// C0741494
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BlazeDemoTest {
	
	//Global driver variable	
		WebDriver driver;	
		
		// location of the chromedriver	
		final String ChromeDriverLocation = "/Users/owner/Desktop/chromedriver";
		
		// website to be tested	
		final String UrlToBeTested = "http://blazedemo.com/index.php" ;


	@Before
	public void setUp() throws Exception {
		//selenium setup
				System.setProperty("webdriver.chrome.driver", ChromeDriverLocation);
				driver = new ChromeDriver();
				
				// go to the website
				driver.get(UrlToBeTested);
	}

	@After
	public void tearDown() throws Exception {
		// After you run the test case, close the browser
					Thread.sleep(5000);
					driver.close();
	}

	@Test
	public void test() {
		// TC1
	}

}
